var controllersMain = angular.module('starter.controllers', []);
controllersMain
.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
   console.log('start chat');
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('ProductsCtrl', function($scope, Products) {
  $scope.products = Products.all();
        console.log('product start', $scope.products );
})

.controller('ProductDetailCtrl', function($scope, $stateParams, Products) {
  $scope.product = Products.get($stateParams.productId);
        console.log('**product detail start', $scope.products );
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  }})
/*
.controller('PizzaSize', function($scope, factoryPizza) {
        //$scope.meats = factoryPizza.all();

        function init(){
            var svc = factoryPizza;
            var i =0;
            svc.all().then(function(data){
                //$scope.echoedVals = data;
                //$scope.meats = data[0].meatAndFish;
                $scope.sizePizza = data[0].sizePizza;
                for(i=0; i<data[0].sizePizza.length;i++){
                console.log('sizePizza found', $scope.sizePizza[i].price );
                }
                //$scope.souse = data[0].souse;
                //$scope.cheese = data[0].cheese;
            }, function(err,s){
                alert(err,s);
            });
        }

    })
  */
    .controller('ProductDetailShop', function($scope, $stateParams, DataSource) {
        $scope.productShop = DataSource.get($stateParams.productId);
        console.log('**product detail start', $scope.productShop );
    });
