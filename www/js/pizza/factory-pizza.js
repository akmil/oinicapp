//data access
appNameMy.factory('factoryPizza', function($http ,$q) {
    var factoryPizza = {};
    var jsonURL = 'http://www.json-generator.com/api/json/get/cbgpJYnybm?indent=2';
    var factoryPizzaArr = [{ name: '_name', id: 0 ,price: 99 , radioModel: '_radioModel', quantity : 0}]; //not used


//factoryPizza.getData = function(){
//        var deferred = $q.defer();
//        $http.get(jsonURL).success(function(data, s){
//            deferred.resolve(data);
//        }).error(function(err, s){
//            deferred.reject(err);
//        });
//        return deferred.promise;
//    };


    //return factoryPizza;
    return {
        all: function(){
            var deferred = $q.defer();
            $http.get(jsonURL).success(function(data, s){
                deferred.resolve(data);
            }).error(function(err, s){
                deferred.reject(err);
            });
            console.log('factoryPizza');
            return deferred.promise;
        },
        get: function(itemId1) {
            // Simple index lookup
            var deferred = $q.defer();
            $http.get(jsonURL).success(function(data, s){
                deferred.resolve(data);
            }).error(function(err, s){
                deferred.reject(err);
            });
            console.log('get: ', deferred.promise.$$state.name);
            return deferred.promise;

            //return items[itemId];
        },
        setPizzaSize: function(_size) {
            factoryPizza.sizeSet = _size;
            console.log('setSize', factoryPizza.sizeSet);
            //return this;
        },
        getPizzaSize: function() {
            return factoryPizza.sizeSet;
        },
        finalPriceSharedModel:{
            factoryPizzaArr:[],
            finalPrice:0
        }
    }
});


