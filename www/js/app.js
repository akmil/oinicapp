// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var appNameMy =
angular.module('starter', ['ionic',
    'starter.controllers',
    'starter.services',
    'ui.bootstrap'//tabs support
])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: "/tab",
    abstract: true,
    templateUrl: "templates/tabs.html"
  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })

      .state('tab.pizza', {
          url: '/pizza',
          views: {
              //tab-pizza@tab.pizza - actual name of state
              'tab-pizza': { //current tab name should be equal for each view
                  templateUrl: 'templates/pizza-create/size-choose.html',
                  controller: 'pizzaConstructorCtrl'
              }
          }
      })
      .state('tab.meat-detail', {
          url: '/pizza/meat',
          views: {
              'tab-pizza': { //current tab name
                  templateUrl: 'templates/pizza-create/meat-detail.html',
                  controller: 'pizzaConstructorCtrl'
              }
          }
      })
      //chees
      .state('tab.cheese-detail', {
          url: '/pizza/cheese',
          views: {
              'tab-pizza': { //current tab name
                  templateUrl: 'templates/pizza-create/chees-detail.html',
                  controller: 'pizzaConstructorCtrl'
              }
          }
      })
      //sous
      .state('tab.sous-detail', {
          url: '/pizza/sous',
          views: {
              'tab-pizza': { //current tab name
                  templateUrl: 'templates/pizza-create/sous-detail.html',
                  controller: 'pizzaConstructorCtrl'
              }
          }
      })


  .state('tab.chats', {
      url: '/chats',
      views: {
        'tab-chats': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'ChatsCtrl'
        }
      }
    })
    .state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })
      //test magento xml parsing
    .state('tab.shop', {
          url: '/shop',
          views: {
              'tab-shop': {
                  templateUrl: 'templates/shopDemo/index.html',
                  controller: 'guitarTestController'
              }
          }
      })
    .state('tab.shop-detail-my', {
          url: '/productDemo/product-detail:product_enityId',
          views: {
              'tab-shop': {
                  templateUrl: 'templates/shopDemo/product-detail.html',
                  controller: 'guitarTestController'
              }
          }
      })
    .state('tab.products', {
      url: '/products',
      views: {
        'tab-products': {
          templateUrl: 'templates/tab-products.html',
          controller: 'ProductsCtrl'
        }
      }
    })
    .state('tab.product-detail', {
      url: '/product/:productId',
      views: {
        'tab-products': {
          templateUrl: 'templates/product-detail.html',
          controller: 'ProductDetailCtrl'
        }
      }
    })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/dash');

});
